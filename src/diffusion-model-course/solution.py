from album.runner.api import setup

notebook_name = '01_introduction_to_diffusers.ipynb'


def install():
    import shutil
    from album.runner.api import get_package_path, get_data_path
    notebook_clean = get_package_path().joinpath(notebook_name)
    notebook_user = get_data_path().joinpath(notebook_name)
    get_data_path().mkdir(parents=True)
    shutil.copyfile(notebook_clean, notebook_user)


def run():
    from album.runner.api import get_args, get_data_path
    import subprocess
    args = get_args()
    notebook_path = get_data_path().joinpath(notebook_name)
    assert (notebook_path.exists())
    subprocess.run(["jupyter", "notebook"], cwd=str(notebook_path.parent))


setup(
    group="de.mdc-berlin",
    name="diffusion-model-course",
    version="0.1.0",
    solution_creators=["Deborah Schmidt"],
    title="Solution running the notebook of the diffusion model course by huggingface",
    album_api_version="0.5.1",
    run=run,
    install=install,
    dependencies={'environment_file': """channels:
- conda-forge
- defaults
dependencies:
- python=3.9
- scikit-image=0.19.1
- notebook=6.5.2
- matplotlib=3.7.2
- torchvision=0.14.1
- diffusers=0.18.2
- datasets=2.13.1
- transformers=4.30.2
- accelerate=0.21.0
- ftfy=6.1.1
- pyarrow=9.0.0
- tifffile=2021.4.8
"""}
)
