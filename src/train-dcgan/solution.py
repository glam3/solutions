from album.runner.api import setup

env_file = """channels:
  - conda-forge
  - defaults
dependencies:
  - python=3.9
  - pytorch
  - torchvision
  - tifffile
  - cudatoolkit=11.0  # change this to match your CUDA version, or remove it if you're not using CUDA
"""

def run():
    from album.runner.api import get_args
    args = get_args()

    # Import required modules inside the run method.
    import os
    import random
    import torch
    import torch.nn as nn
    import torch.backends.cudnn as cudnn
    import torch.optim as optim
    import torch.utils.data
    import torchvision.datasets as dset
    import torchvision.transforms as transforms
    import torchvision.utils as vutils
    import numpy as np
    from torchvision.transforms import RandomHorizontalFlip, RandomVerticalFlip

    # Your script starts here
    opt = args

    cudnn.benchmark = True

    try:
        os.makedirs(opt.outf)
    except OSError:
        pass

    if opt.manualSeed is None:
        opt.manualSeed = random.randint(1, 10000)
    print("Random Seed: ", opt.manualSeed)
    random.seed(opt.manualSeed)
    torch.manual_seed(opt.manualSeed)

    cudnn.benchmark = True

    if torch.cuda.is_available() and not opt.cuda:
        print("WARNING: You have a CUDA device, so you should probably run with --cuda")

    if torch.backends.mps.is_available() and not opt.mps:
        print("WARNING: You have mps device, to enable macOS GPU run with --mps")

    # Import additional libraries
    import os
    import tifffile


    # Create a custom dataset class for 3D TIFF files
    class Tiff3DSlicesDataset(torch.utils.data.Dataset):
        def __init__(self, data_folder, transform=None):
            self.data_folder = data_folder
            self.transform = transform
            self.file_list = [f for f in os.listdir(data_folder) if f.endswith('.tif')]
            self.volumes = []
            for i in range(len(self.file_list)):
                file_path = os.path.join(self.data_folder, self.file_list[i])
                with tifffile.TiffFile(file_path) as tif:
                    tiff_volume = tif.asarray()
                tiff_volume = tiff_volume.astype(np.uint16)
                # tiff_volume /= 65535.0  # Normalize 16-bit image
                tiff_volume = np.squeeze(tiff_volume)
                self.volumes.append(tiff_volume)

        def __len__(self):
            return len(self.file_list)*100

        def __getitem__(self, index):
            from PIL import Image
            slinces = []
            for i in range(20):
                volume = self.volumes[int(index / 100)]
                slice_index = np.random.randint(volume.shape[0])  # Randomly select a slice
                img_slice = volume[slice_index, :, :]
                # img_slice = np.stack([img_slice, img_slice, img_slice], axis=-1)
                img_slice = Image.fromarray(img_slice, 'I;16')
                img_slice = img_slice.convert('F')
                # img_slice = Image.fromarray(img_slice, 'RGB')

                if self.transform:
                    img_slice = self.transform(img_slice)
                slinces.append(img_slice)

            return slinces

    # Replace the original dataset with the custom dataset class in the DataLoader
    dataset = Tiff3DSlicesDataset(opt.data_folder, transform=transforms.Compose([
        transforms.RandomRotation(degrees=180),  # Rotate the image randomly by up to 30 degrees
        transforms.RandomCrop(size=opt.imageSize),  # Randomly crop the image
        transforms.RandomHorizontalFlip(p=0.5),  # Add this line
        transforms.RandomVerticalFlip(p=0.5),  # Add this line
        transforms.ToTensor(),
        transforms.Normalize((0.5,), (0.5,)),
    ]))

    nc=1

    assert dataset
    dataloader = torch.utils.data.DataLoader(dataset, batch_size=opt.batchSize,
                                             shuffle=True, num_workers=int(opt.workers))

    use_mps = opt.mps and torch.backends.mps.is_available()
    if opt.cuda:
        device = torch.device("cuda:0")
    elif use_mps:
        device = torch.device("mps")
    else:
        device = torch.device("cpu")

    ngpu = int(opt.ngpu)
    nz = int(opt.nz)
    ngf = int(opt.ngf)
    ndf = int(opt.ndf)

    # custom weights initialization called on netG and netD
    def weights_init(m):
        classname = m.__class__.__name__
        if classname.find('Conv') != -1:
            torch.nn.init.normal_(m.weight, 0.0, 0.02)
        elif classname.find('BatchNorm') != -1:
            torch.nn.init.normal_(m.weight, 1.0, 0.02)
            torch.nn.init.zeros_(m.bias)

    class Generator(nn.Module):
        def __init__(self, ngpu):
            super(Generator, self).__init__()
            self.ngpu = ngpu
            self.main = nn.Sequential(
                # input is Z, going into a convolution
                nn.ConvTranspose2d(nz, ngf * 8, 4, 1, 0, bias=False),
                nn.BatchNorm2d(ngf * 8),
                nn.ReLU(True),
                # state size. (ngf*8) x 4 x 4
                nn.ConvTranspose2d(ngf * 8, ngf * 4, 4, 2, 1, bias=False),
                nn.BatchNorm2d(ngf * 4),
                nn.ReLU(True),
                # state size. (ngf*4) x 8 x 8
                nn.ConvTranspose2d(ngf * 4, ngf * 2, 4, 2, 1, bias=False),
                nn.BatchNorm2d(ngf * 2),
                nn.ReLU(True),
                # state size. (ngf*2) x 16 x 16
                nn.ConvTranspose2d(ngf * 2, ngf, 4, 2, 1, bias=False),
                nn.BatchNorm2d(ngf),
                nn.ReLU(True),
                # state size. (ngf) x 32 x 32
                nn.ConvTranspose2d(ngf, nc, 4, 2, 1, bias=False),
                nn.Tanh()
                # state size. (nc) x 64 x 64
            )

        def forward(self, input):
            if input.is_cuda and self.ngpu > 1:
                output = nn.parallel.data_parallel(self.main, input, range(self.ngpu))
            else:
                output = self.main(input)
            return output

    netG = Generator(ngpu).to(device)
    netG.apply(weights_init)
    if opt.netG != '':
        netG.load_state_dict(torch.load(opt.netG))
    print(netG)

    class Discriminator(nn.Module):
        def __init__(self, ngpu):
            super(Discriminator, self).__init__()
            self.ngpu = ngpu
            self.main = nn.Sequential(
                # input is (nc) x 64 x 64
                nn.Conv2d(nc, ndf, 4, 2, 1, bias=False),
                nn.LeakyReLU(0.2, inplace=True),
                # state size. (ndf) x 32 x 32
                nn.Conv2d(ndf, ndf * 2, 4, 2, 1, bias=False),
                nn.BatchNorm2d(ndf * 2),
                nn.LeakyReLU(0.2, inplace=True),
                # state size. (ndf*2) x 16 x 16
                nn.Conv2d(ndf * 2, ndf * 4, 4, 2, 1, bias=False),
                nn.BatchNorm2d(ndf * 4),
                nn.LeakyReLU(0.2, inplace=True),
                # state size. (ndf*4) x 8 x 8
                nn.Conv2d(ndf * 4, ndf * 8, 4, 2, 1, bias=False),
                nn.BatchNorm2d(ndf * 8),
                nn.LeakyReLU(0.2, inplace=True),
                # state size. (ndf*8) x 4 x 4
                nn.Conv2d(ndf * 8, 1, 4, 1, 0, bias=False),
                nn.Sigmoid()
            )

        def forward(self, input):
            if input.is_cuda and self.ngpu > 1:
                output = nn.parallel.data_parallel(self.main, input, range(self.ngpu))
            else:
                output = self.main(input)

            return output.view(-1, 1).squeeze(1)

    netD = Discriminator(ngpu).to(device)
    netD.apply(weights_init)
    if opt.netD != '':
        netD.load_state_dict(torch.load(opt.netD))
    print(netD)

    criterion = nn.BCELoss()

    fixed_noise = torch.randn(opt.batchSize, nz, 1, 1, device=device)
    real_label = 1
    fake_label = 0

    # setup optimizer
    optimizerD = optim.Adam(netD.parameters(), lr=opt.lr, betas=(opt.beta1, 0.999))
    optimizerG = optim.Adam(netG.parameters(), lr=opt.lr, betas=(opt.beta1, 0.999))

    if opt.dry_run:
        opt.niter = 1

    for epoch in range(opt.niter):
        for i, data in enumerate(dataloader, 0):
            ############################
            # (1) Update D network: maximize log(D(x)) + log(1 - D(G(z)))
            ###########################
            # train with real
            netD.zero_grad()
            real_cpu = data[0].to(device)
            batch_size = real_cpu.size(0)
            label = torch.full((batch_size,), real_label,
                               dtype=real_cpu.dtype, device=device)

            output = netD(real_cpu)
            errD_real = criterion(output, label)
            errD_real.backward()
            D_x = output.mean().item()

            # train with fake
            noise = torch.randn(batch_size, nz, 1, 1, device=device)
            fake = netG(noise)
            label.fill_(fake_label)
            output = netD(fake.detach())
            errD_fake = criterion(output, label)
            errD_fake.backward()
            D_G_z1 = output.mean().item()
            errD = errD_real + errD_fake
            optimizerD.step()

            ############################
            # (2) Update G network: maximize log(D(G(z)))
            ###########################
            netG.zero_grad()
            label.fill_(real_label)  # fake labels are real for generator cost
            output = netD(fake)
            errG = criterion(output, label)
            errG.backward()
            D_G_z2 = output.mean().item()
            optimizerG.step()

            print('[%d/%d][%d/%d] Loss_D: %.4f Loss_G: %.4f D(x): %.4f D(G(z)): %.4f / %.4f'
                  % (epoch, opt.niter, i, len(dataloader),
                     errD.item(), errG.item(), D_x, D_G_z1, D_G_z2))
            if i % 100 == 0:
                vutils.save_image(real_cpu,
                                  '%s/real_samples.png' % opt.outf,
                                  normalize=True)
                fake = netG(fixed_noise)
                vutils.save_image(fake.detach(),
                                  '%s/fake_samples_epoch_%03d.png' % (opt.outf, epoch),
                                  normalize=True)

            if opt.dry_run:
                break
        if epoch % 20 == 0:
            # do checkpointing
            torch.save(netG.state_dict(), '%s/netG_epoch_%d.pth' % (opt.outf, epoch))
            torch.save(netD.state_dict(), '%s/netD_epoch_%d.pth' % (opt.outf, epoch))


setup(
    group="de.mdc-berlin",
    name="glam-train",
    version="0.1.0",
    title="DCGAN PyTorch Example",
    description="An Album solution for running a Deep Convolutional Generative Adversarial Network (DCGAN) in PyTorch.",
    authors=["Your Name"],
    cite=[],
    tags=["dcgan", "pytorch"],
    license="MIT",
    documentation=["documentation.md"],
    covers=[{
        "description": "Dummy cover image.",
        "source": "cover.png"
    }],
    album_api_version="0.3.1",
    args=[{
            "name": "workers",
            "type": "integer",
            "required": False,
            "default": 2,
            "description": "Number of data loading workers"
        },
        {
            "name": "batchSize",
            "type": "integer",
            "required": False,
            "default": 64,
            "description": "Input batch size"
        },
        {
            "name": "imageSize",
            "type": "integer",
            "required": False,
            "default": 64,
            "description": "The height / width of the input image to network"
        },
        {
            "name": "nz",
            "type": "integer",
            "required": False,
            "default": 100,
            "description": "Size of the latent z vector"
        },
        {
            "name": "ngf",
            "type": "integer",
            "required": False,
            "default": 64,
            "description": "Number of generator filters in first conv layer"
        },
        {
            "name": "ndf",
            "type": "integer",
            "required": False,
            "default": 64,
            "description": "Number of discriminator filters in first conv layer"
        },
        {
            "name": "niter",
            "type": "integer",
            "required": False,
            "default": 25,
            "description": "Number of training epochs"
        },
        {
            "name": "lr",
            "type": "float",
            "required": False,
            "default": 0.0002,
            "description": "Learning rate, default=0.0002"
        },
        {
            "name": "beta1",
            "type": "float",
            "required": False,
            "default": 0.5,
            "description": "Beta1 hyperparameter for Adam optimizers, default=0.5"
        },
        {
            "name": "cuda",
            "type": "boolean",
            "required": False,
            "default": False,
            "description": "Enable CUDA"
        },
        {
            "name": "ngpu",
            "type": "integer",
            "required": False,
            "default": 1,
            "description": "Number of GPUs available, use 0 for CPU mode"
        },
        {
            "name": "netG",
            "type": "string",
            "required": False,
            "default": "",
            "description": "Path to netG (to continue training)"
        },
        {
            "name": "netD",
            "type": "string",
            "required": False,
            "default": "",
            "description": "Path to netD (to continue training)"
        },
        {
            "name": "outf",
            "type": "string",
            "required": False,
            "default": "results",
            "description": "Folder to output images and model checkpoints"
        },
        {
            "name": "manualSeed",
            "type": "integer",
            "required": False,
            "description": "Manual seed value, set to -1 for random seed"
        },
        {
            "name": "classes",
            "type": "string",
            "required": False,
            "default": "",
            "description": "Comma-separated list of classes for the dataset"
        },
        {
            "name": "mps",
            "type": "boolean",
            "default": False,
            "description": "enables macOS GPU training"
        },
        {
            "name": "dry-run",
            "type": "boolean",
            "default": False,
            "description": "check a single training cycle works"
        },
        {
            "name": "data_folder",
            "type": "string",
            "required": True,
            "description": "Path to the folder containing 3D TIFF datasets"
        },
    ],
    dependencies={
        "environment_file": env_file,
    },
    run=run
)


